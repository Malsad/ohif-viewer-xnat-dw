window.config = {
  routerBasename: '/',
  showStudyList: true,
  servers: {
    dicomWeb: [
      {
        name: 'XNAT-DICOMWeb',
        wadoUriRoot: 'xapi/dicomweb',
        qidoRoot: 'xapi/dicomweb',
        wadoRoot: 'xapi/dicomweb',
        qidoSupportsIncludeField: false,
        imageRendering: 'wadors',
        thumbnailRendering: 'wadors',
      },
    ],
  },
};
